import * as Errors from '../../common/errors';
import * as Finals from '../../common/finals';

export default class UsersDao {
    constructor(appContext) {
        this.appContext = appContext;
    }

    async create(user) {
        const { fullName, username, email, password } = user;

        if (!fullName || !username || !email || !password) {
            throw new Error(Errors.INVALID_ATTRIBUTE);
        }

        let con = null;
        try {
            con = await this.appContext.getConnection();

            const result = await new Promise((resolve, reject) => {
                const query = {
                    sql: `INSERT INTO ${Finals.USERS_TABLE} (fullName, username, email, password) VALUES (?, ?, ?, ?)`,
                    values: [fullName, username, email, password]
                }

                con.query(query, (err,result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });
            });

            return result;
        } catch (e) {
            throw new Error(Errors.DB_ERROR(e.message));
        } finally {
            if (con) {
                con.release();
            }
        }
    }

    async auth(authData) {
        const { username, password } = authData;

        if (!username || !password) {
            throw new Error(Errors.INVALID_ATTRIBUTE);
        }

        let con = null;
        try {
            con = await this.appContext.getConnection();

            const results = await new Promise((resolve, reject) => {
                const query = {
                    sql: `SELECT * FROM ${Finals.USERS_TABLE} WHERE username=? AND password=?`,
                    values: [username, password]
                }

                con.query(query, (err,results) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(results);
                    }
                });
            });

            return results.length > 0 ? results[0] : null;
        } catch (e) {
            throw new Error(Errors.DB_ERROR(e.message));
        } finally {
            if (con) {
                con.release();
            }
        }
    }
}
