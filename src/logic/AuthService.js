import UsersDao from '../persistence/users/UsersDao';
import * as Errors from "../common/errors";
import { HashUtil } from "../utils";

export default class AuthService {
    constructor(appContext) {
        this.usersDao = new UsersDao(appContext);
    }

    async auth(auth) {
        const { username, password } = auth;

        if (!username || !password) {
            throw new Error(Errors.INVALID_ATTRIBUTE);
        }

        try {
            const foundUser = await this.usersDao.auth({
                ...auth,
                password: HashUtil.hashPassword(password)
            });

            if (!foundUser) {
                throw new Error(Errors.USERNAME_OR_PASSWORD_INVALID);
            }

            return HashUtil.hashAccessToken(foundUser.username);
        } catch (e) {
            throw new Error(Errors.USER_AUTH_FAILED(e.message));
        }
    };
}
