import UsersDao from '../persistence/users/UsersDao';
import { HashUtil, ValidationsUtil } from '../utils';
import * as Errors from '../common/errors';

export default class UsersService {
    constructor(appContext) {
        this.usersDao = new UsersDao(appContext);
    }

    async register(user) {
        const { fullName, username, email, password } = user;

        if (!fullName || !username || !email || !password) {
            throw new Error(Errors.INVALID_ATTRIBUTE);
        }

        if (!ValidationsUtil.emailValidate(email)) {
            throw new Error(Errors.EMAIL_NOT_VALID);
        }

        try {
            await this.usersDao.create({
                ...user,
                password: HashUtil.hashPassword(password)
            });

            return HashUtil.hashAccessToken(username);
        } catch (e) {
            throw new Error(Errors.USER_REGISTER_FAILED(e.message));
        }
    }
}
