import { RandomsUtil } from '../utils';
import * as Finals from '../common/finals';

export default class MessagesService {
    constructor(appContext) {
        this.appContext = appContext;
    }

    // “spin” - send a message to a random user
    async spin(msg) {
        msg = msg || '';

        const connectedClientsIds = await this._getConnectedClientsIds(this.appContext);

        const randomClientIndex = RandomsUtil.randomInt(0, connectedClientsIds.length);
        const connectedClientId = connectedClientsIds[randomClientIndex];

        this.appContext.io.to(connectedClientId).emit(Finals.MSG, msg);

        return connectedClientId;
    };

    // “wild” - send a message to X random users. X will be determined by the client.
    async wild(msg, x) {
        msg = msg || '';
        x = x || 1;

        const connectedClientsIds = await this._getConnectedClientsIds(this.appContext);

        const randomClientIndexes = RandomsUtil.randomInts(0, connectedClientsIds.length, x);

        const randomClientIds = randomClientIndexes.reduce((acc, randomClientIndex) => {
            const connectedClientId = connectedClientsIds[randomClientIndex];
            this.appContext.io.to(connectedClientId).emit(Finals.MSG, msg);
            acc.push(connectedClientId);
            return acc;
        }, []);

        return randomClientIds;
    };

    // “blast” - sending to all connected clients
    blast(msg) {
        msg = msg || '';

        this.appContext.io.emit(Finals.MSG, msg);
    };

    async _getConnectedClientsIds(appContext) {
        const connectedClientsIds = await new Promise((resolve, reject) => {
            appContext.io.of('/').adapter.clients((error, clients) => {
               if (error) {
                   reject(error);
               } else {
                   resolve(clients);
               }
            });
        });

        return connectedClientsIds;
    }
}
