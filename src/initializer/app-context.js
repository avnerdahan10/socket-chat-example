import express, { Router } from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import http from 'http';
import config from 'config';
import mysql from 'mysql';
import redisAdapter from 'socket.io-redis';
import { ApiRoutes } from '../api/routes';
import * as Finals from '../common/finals';

export default class AppContext {
    init() {
        this.pool = this._persistenceInit();
        this.express = this._expressInit();
        this.server = http.createServer(this.express);

        this._socketIoInit();
        this._routesInit();
        this._restServerInit();
    }

    async getConnection() {
        return await new Promise((resolve, reject) => {
            this.pool.getConnection((err, con) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(con);
                }
            });
        });
    };

    _persistenceInit() {
        try {
            return mysql.createPool(config.mysql);
        } catch (e) {
            throw e;
        }
    }

    _expressInit() {
        const expressApp = express();

        // Parse incoming request bodies
        // https://github.com/expressjs/body-parser
        expressApp.use(bodyParser.json());
        expressApp.use(bodyParser.urlencoded({ extended: true }));

        return expressApp;
    }

    _socketIoInit() {
        const { host, port } =  config.redis;
        this.io = require('socket.io')(this.server);
        this.io.adapter(redisAdapter({ host, port }));

        this.io.on('connection', (socket) => {
            socket.on(Finals.MSG, (msg) => {
                this.io.emit(Finals.MSG, msg);
            });
        });
    }

    _routesInit() {
        const { apiPrefix } = config.server;

        this.express.get('/', (req, res) => {
            const absPath = path.resolve('static/index.html');
            res.sendFile(absPath);
        });

        const apiRoutes = ApiRoutes.getRoutes(this);
        this.express.use(apiPrefix, apiRoutes);
    }

    _restServerInit() {
        const { port } = config.server;

        this.server.listen(port, () => {
            console.log(`listening on *: ${port}`);
        });
    }
}
