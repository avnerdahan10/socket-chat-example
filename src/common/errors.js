export const INVALID_ATTRIBUTE = 'Invalid Attribute';
export const EMAIL_NOT_VALID = 'Email is not valid';

export const USER_REGISTER_FAILED = (e) => `User registration failed, error={${e}}`;
export const USER_AUTH_FAILED = (e) => `User authentication failed, error={${e}}`;
export const USERNAME_OR_PASSWORD_INVALID = `Username or Password invalid`;

export const DB_ERROR = (e) => `DB Error, error={${e}}`;
