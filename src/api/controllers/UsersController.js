import { UsersService } from '../../logic';

export default class UsersController {
    constructor(appContext) {
        this.usersService = new UsersService(appContext);
    }

    register = async (req, res, next) => {
        const { body } = req;

        try {
            const registeredUser = await this.usersService.register(body);
            return res.status(200).json({ data: registeredUser });
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    };
}

