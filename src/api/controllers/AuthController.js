import { AuthService } from '../../logic';

export default class AuthController {
    constructor(appContext) {
        this.authService = new AuthService(appContext);
    }

    auth = async (req, res, next) => {
        const { body } = req;

        try {
            const token = await this.authService.auth(body);
            return res.status(200).json({ data: token });
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    };
}

