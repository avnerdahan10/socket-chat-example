import { MessagesService } from '../../logic';

export default class MessagesController {
    constructor(appContext) {
        this.messagesService = new MessagesService(appContext);
    }

    spin = async (req, res, next) => {
        const { msg } = req.body;

        try {
            const clientId = await this.messagesService.spin(msg);

            const data = clientId
                ? `Spin was sent to connected client '${clientId}'`
                : `Spin wasn't sent - there are 0 connected client`;

            return res.status(200).json({ data });
        } catch (e) {
            return res.status(500).json({ message: `Spin - FAILED, error=${e.message}` });
        }
    };

    wild = async (req, res, next) => {
        const { msg, x } = req.body;

        try {
            const clientIds = await this.messagesService.wild(msg, x);

            const data = clientIds
                ? `Wild was sent to connected clients '${JSON.stringify(clientIds)}'`
                : `Wild wasn't sent - there are 0 connected client`;

            return res.status(200).json({ data });

            return res.status(200).json({ data: `Wild was sent to connected client '${JSON.stringify(clientIds)}'` });
        } catch (e) {
            return res.status(500).json({ message: `Wild - FAILED, error=${e.message}` });
        }
    };

    blast = async (req, res, next) => {
        const { msg } = req.body;

        try {
            await this.messagesService.blast(msg);
            return res.status(200).json({ data: `Blast was sent to all connected clients` });
        } catch (e) {
            return res.status(500).json({ message: `Blast - FAILED, error=${e.message}` });
        }
    };
}

