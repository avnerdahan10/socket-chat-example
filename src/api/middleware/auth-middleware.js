import jwt from 'jsonwebtoken';
import config from 'config';
import * as Finals from '../../common/finals';

export default class AuthMiddleware {
    auth = (req, res, next) => {
        const accessToken = req.headers[Finals.X_ACCESS_TOKEN];

        if (!accessToken) {
            return res.status(401).json(this._buildError());
        }

        const { app: { jwtSecret } } = config;
        try {
            // invalid token - synchronous
            const decoded = jwt.verify(accessToken, jwtSecret);
            return next();
        } catch(e) {
            return res.status(401).json(this._buildError());
        }
    };

    _buildError() {
        return {
            message: 'User unauthorized or token is expired'
        };
    }
}
