import { Router } from 'express';
import {
    AuthMiddleware,
} from '../middleware';
import {
    UsersController,
    MessagesController,
    AuthController
} from '../controllers';

export default class ApiRoutes {
    static getRoutes(appContext) {
        try {
            const usersController = new UsersController(appContext);
            const messagesController = new MessagesController(appContext);
            const authController = new AuthController(appContext);

            const authMiddleware = new AuthMiddleware();

            const routes = new Router();

            routes.post('/messages/spin', authMiddleware.auth, messagesController.spin);
            routes.post('/messages/wild', authMiddleware.auth, messagesController.wild);
            routes.post('/messages/blast', authMiddleware.auth, messagesController.blast);

            routes.post('/users', usersController.register);

            routes.post('/auth', authController.auth);

            return routes;
        }
        catch (e) {
            console.error('Exception in ApiRoutes.js', e.message);
        }
    }
}
