import { AppContext } from './initializer';

try {
    const appContext = new AppContext();
    appContext.init()
} catch (e) {
    console.error('Can not initialize socket-chat-example', e.message);
    process.exit(1);
}
