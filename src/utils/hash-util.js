import config from 'config';
import hash from 'hash.js';
import jwt from 'jsonwebtoken';

export default class HashUtil {
    static hashPassword(pass) {
        const { app: { passwordSalt }  } = config;
        if (!pass) {
            return null;
        }

        return hash.sha256().update(pass + passwordSalt).digest('hex');
    }

    static hashAccessToken(username) {
        const { app: { jwtSecret, accessTokenExp }  } = config;

        const options = {
            expiresIn: accessTokenExp
        };

        return jwt.sign({ username }, jwtSecret, options);
    }
}
