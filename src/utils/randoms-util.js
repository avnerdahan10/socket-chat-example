export default class RandomsUtil {
    static randomInt(min, max) {
        return Math.floor(Math.random() * Math.floor(max)) + min;
    }

    static randomInts(min, max, x) {
        const res = [];

        if ((max - min) < x) {
            x = (max - min);
        }

        while(res.length < x){
            const randomInt = RandomsUtil.randomInt(min, max);
            if(res.indexOf(randomInt) === -1) {
                res.push(randomInt);
            }
        }

        return res;
    }
}
