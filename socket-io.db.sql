CREATE SCHEMA `socket-io-db` ;

CREATE TABLE `socket-io-db`.`users` (
  `id` INT AUTO_INCREMENT,
  `fullName` VARCHAR(255) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `EMAIL_UNIQUE` (`email` ASC),
  UNIQUE INDEX `USERNAME_UNIQUE` (`username` ASC));
